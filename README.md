jQuery touchSwipe Component
===========================

Shim repository for the [jQuery touchSwipe](http://labs.rampinteractive.co.uk/touchSwipe/demos/index.html).

Package Managers
----------------

* [Component](https://github.com/component/component): `immae/jquery-touchswipe`
* [Composer](http://packagist.org/packages/components/jquery): `immae/jquery-touchswipe`
